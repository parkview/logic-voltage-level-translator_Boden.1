Project Name:  Boden - Data Level Translator
Project date: 2021-04-27


This PCB will be handy for people wanting to drive a Neopixel WS2812 LED from a 3V or 3.3V logic level board, ie: ESP32, Microbit etc.  It will fit into a breadboard.

This board has one data in and one data out.


PCB Versions:
-------------

Version: 1.0
Production:  JLCPCB, White, 1.6mm, 2 layer
Production Date: 2021-04-??

* initial design




